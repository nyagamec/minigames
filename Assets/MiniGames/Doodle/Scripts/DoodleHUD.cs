using System.Threading.Tasks;
using UnityEngine;
using TMPro;

public class DoodleHUD : SceneSingleton<DoodleHUD> {
    [SerializeField] private TextMeshProUGUI distanceTxt;
    [SerializeField] private TextMeshProUGUI coinsTxt;
    [SerializeField] private TextMeshProUGUI gameOverTxt;

    protected new void Awake() {
        base.Awake();
        DoodleEvents.Instance.OnUpdateDistance += UpdateDistance;
        DoodleEvents.Instance.OnUpdateCoins += UpdateCoins;
        DoodleEvents.Instance.OnEndGame += ShowGameOver;
    }

    private void UpdateDistance(int distance) {
        distanceTxt.text = distance.ToString();
    }

    private void UpdateCoins(int coins) {
        coinsTxt.text = coins.ToString();
    }

    private async void ShowGameOver() {
        gameOverTxt.enabled = true;
        TimeController.Instance.Type = TimeController.TimeType.Stop;
        TimeController.Instance.Type = TimeController.TimeType.Stabile;
        GameScenes.Instance.Scene = GameScenes.SceneName.Hub;
        await Task.Delay(1000);
    }
}
