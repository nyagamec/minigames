using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Doodler : MonoBehaviour, ITick {
    [SerializeField] private float powerJump;
    [SerializeField] private float moveSpeed;

    private Vector2 vector;
    private Rigidbody2D rb;
    
    private void OnEnable() => TickController.Add(this);
    private void OnDisable() => TickController.Remove(this);

    private void Awake() {
        GameSettings.Instance.hero = transform;
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if(collision.transform.tag == "Platform") {
            rb.velocity = Vector2.zero;
            rb.AddForce(Vector2.up * powerJump);
            Mathf.Clamp(rb.velocity.y, 0, powerJump);
        }
    }

    public void Tick() {
        Move();
        Teleport();
        UpdateDistance();
        TryDead();
    }

    private void Move() {
        vector = rb.velocity;
        vector.x = Input.GetAxis("Horizontal") * moveSpeed;
        rb.velocity = vector;
    }

    private void Teleport() {
        if(transform.position.x > GameSettings.Instance.borders.right) {
            vector = transform.position;
            vector.x = GameSettings.Instance.borders.left;
            transform.position = vector;
        }else if(transform.position.x < GameSettings.Instance.borders.left) {
            vector = transform.position;
            vector.x = GameSettings.Instance.borders.right;
            transform.position = vector;
        }
    }

    private void UpdateDistance() {
        if (DoodleHolder.Instance.MaxDistance < transform.position.y) {
            DoodleHolder.Instance.MaxDistance = transform.position.y;
        }
    }

    private void TryDead() {
        if(transform.position.y < GameSettings.Instance.borders.bottom) {
            DoodleEvents.Instance.EndGame();
        }
    }
}
