using UnityEngine;

public class DoodleSettings : SceneSingleton<DoodleSettings> {
    public Vector2 width;
    public float bottom;
    
    protected new void Awake() {
        base.Awake();
        TimeController.Instance.Type = TimeController.TimeType.MoveByFunction;
        GameSettings.Instance.borders.left = width.x;
        GameSettings.Instance.borders.right = width.y;
        GameSettings.Instance.borders.bottom = bottom;
    }
}
