using System.Threading.Tasks;
using UnityEngine;

public class DoodlePlatformController : MonoBehaviour {
    [SerializeField] private DoodlePlatform prefab;
    [SerializeField] private int count;
    [SerializeField] private float startSpawnDistance;
    [SerializeField] private Vector2 distance;

    private Vector2 nextPosition;
    
    private void Awake() {
        nextPosition.y = startSpawnDistance;
        nextPosition.x = Random.Range(GameSettings.Instance.borders.left + prefab.transform.localScale.x / 2,
                                      GameSettings.Instance.borders.right - prefab.transform.localScale.x / 2);
        for(int i = 0; i < count; ++i) {
            SpawnPlatform();
        }
    }

    private void SpawnPlatform() {
        GameSettings.Instance.borders.top = nextPosition.y;
        DoodlePlatform platform = Instantiate(prefab, nextPosition, Quaternion.identity);
        platform.OnHide += SpawnPlatform;
        CalcNewPosition();
    }

    private void CalcNewPosition() {
        nextPosition.y += Random.Range(distance.x, distance.y);
        nextPosition.x = Random.Range(GameSettings.Instance.borders.left + prefab.transform.localScale.x / 2,
                                      GameSettings.Instance.borders.right - prefab.transform.localScale.x / 2);
    }
}
