public class DoodleHolder : SceneSingleton<DoodleHolder> {
    private void Start() {
        GameEvents.Instance.OnAddCoin += () => ++Coins;
    }

    private float maxDistance;
    public float MaxDistance {
        get => maxDistance;
        set {
            maxDistance = value;
            DoodleEvents.Instance.UpdateDistance((int)value);
        }
    }

    private int coins;
    public int Coins {
        get => coins;
        set {
            coins = value;
            DoodleEvents.Instance.UpdateCoins(value);
        }
    }
}
