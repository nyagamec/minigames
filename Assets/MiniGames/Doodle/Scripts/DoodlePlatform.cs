using UnityEngine;
using System;

public class DoodlePlatform : MonoBehaviour, ITick {
    [SerializeField] private float distanceToHide;

    public Action OnHide;

    private void OnEnable() => TickController.Add(this);
    private void OnDisable() => TickController.Remove(this);

    public void Tick() {
        TryHide();
    }

    private void TryHide() {
        if(GameSettings.Instance.hero.position.y > transform.position.y + distanceToHide) {
            Hide();
        }
    }

    private void Hide() {
        GameSettings.Instance.borders.bottom = transform.position.y;
        OnHide?.Invoke();
        Destroy(gameObject);
    }
}
