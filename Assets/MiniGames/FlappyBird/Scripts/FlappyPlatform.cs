using UnityEngine;
using System;

public class FlappyPlatform : MonoBehaviour, ITick {
    [SerializeField] private FlappyPlatformElement top;
    [SerializeField] private FlappyPlatformElement bottom;
    [SerializeField] private float distanceToHide;

    public Action OnHide;

    private Vector2 vector;

    public float Position {
        get => transform.position.x;
        set {
            vector = transform.position;
            vector.x = value;
            transform.position = vector;
        }
    }
    public float Width {
        get => transform.localScale.x;
        set {
            vector = transform.localScale;
            vector.x = value;
            transform.localScale = vector;
        }
    }

    public float TopPosition {
        get => top.Position;
        set => top.Position = value;
    }
    public float TopHeight {
        get => top.Height;
        set => top.Height = value;
    }

    public float BottomPosition {
        get => bottom.Position;
        set => bottom.Position = value;
    }
    public float BottomHeight {
        get => bottom.Height;
        set => bottom.Height = value;
    }

    private void OnEnable() => TickController.Add(this);
    private void OnDisable() => TickController.Remove(this);

    public void Tick() {
        TryHide();
    }

    private void TryHide() {
        if (GameSettings.Instance.hero.position.x > transform.position.x + distanceToHide) {
            Hide();
        }
    }

    private void Hide() {
        GameSettings.Instance.borders.left = transform.position.x;
        OnHide?.Invoke();
        Destroy(gameObject);
    }
}
