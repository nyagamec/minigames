using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlappyPlatformElement : MonoBehaviour {
    private Vector2 vector;
    public float Position {
        get => transform.position.y;
        set {
            vector = transform.position;
            vector.y = value;
            transform.position = vector;
        }
    }
    public float Height {
        get => transform.localScale.y;
        set {
            vector = transform.localScale;
            vector.y = value;
            transform.localScale = vector;
        }
    }
}
