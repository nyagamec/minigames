using UnityEngine;

public class FlappySettings : SceneSingleton<FlappySettings> {
    public Vector2 height;
    public float left;

    protected new void Awake() {
        base.Awake();
        TimeController.Instance.Type = TimeController.TimeType.MoveByCurve;
        GameSettings.Instance.borders.bottom = height.x;
        GameSettings.Instance.borders.top = height.y;
        GameSettings.Instance.borders.left = left;
    }
}
