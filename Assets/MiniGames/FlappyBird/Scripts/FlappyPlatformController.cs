using UnityEngine;

public class FlappyPlatformController : MonoBehaviour {
    [SerializeField] private FlappyPlatform prefab;
    [SerializeField] private int count;
    [SerializeField] private float startSpawnDistance;
    [SerializeField] private Vector2 rangeHolePositionY;
    [SerializeField] private Vector2 rangeHoleHeight;
    [SerializeField] private Vector2 rangeDistance;
    [SerializeField] private Vector2 rangeWidth;

    private FlappyPlatform platform;
    private Vector2 holePosition;
    private float holeHeight;
    private float oldWidth;
    private float position;
    private float width;

    private void Start() {
        width = Random.Range(rangeWidth.x, rangeWidth.y);
        position = startSpawnDistance;
        
        for(int i = 0; i < count; ++i) {
            SpawnPlatform();
        }
    }

    private void SpawnPlatform() {
        platform = Instantiate(prefab);
        platform.OnHide += SpawnPlatform;
        UpdateHoleElementPlatform();
        UpdateTopElementPlatform();
        UpdateBottomElementPlatform();
        UpdatePlatformTransform();
        CalcNewTransform();
    }

    private void UpdateHoleElementPlatform() {
        holePosition.y = Random.Range(rangeHolePositionY.x, rangeHolePositionY.y);
        holeHeight = Random.Range(rangeHoleHeight.x, rangeHoleHeight.y);
    }

    private void UpdateTopElementPlatform() {
        platform.TopHeight = GameSettings.Instance.borders.top - holePosition.y - holeHeight / 2;
        platform.TopPosition = GameSettings.Instance.borders.top - platform.TopHeight / 2;
    }

    private void UpdateBottomElementPlatform() {
        platform.BottomHeight = holePosition.y - holeHeight / 2 - GameSettings.Instance.borders.bottom;
        platform.BottomPosition = GameSettings.Instance.borders.bottom + platform.BottomHeight / 2;
    }

    private void UpdatePlatformTransform() {
        platform.Position = position;
        platform.Width = width;
    }

    private void CalcNewTransform() {
        width = Random.Range(rangeWidth.x, rangeWidth.y);
        oldWidth = platform.Width;
        position += (oldWidth + platform.Width) / 2 + Random.Range(rangeDistance.x, rangeDistance.y);
        GameSettings.Instance.borders.right = position;
    }
}
