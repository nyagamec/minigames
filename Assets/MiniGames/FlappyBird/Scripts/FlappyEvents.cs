using System;

public class FlappyEvents : SceneSingleton<FlappyEvents> {
    public void UpdateDistance(int distance) => OnUpdateDistance?.Invoke(distance);
    public event Action<int> OnUpdateDistance;
    public void UpdateCoins(int count) => OnUpdateCoins?.Invoke(count);
    public event Action<int> OnUpdateCoins;
    public void EndGame() => OnEndGame?.Invoke();
    public event Action OnEndGame;
}
