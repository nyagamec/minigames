using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Bird : MonoBehaviour, ITick {
    [SerializeField] private float powerJump;
    [SerializeField] private float moveSpeed;
    
    private Rigidbody2D rb;
    private Vector2 vector;

    private void OnEnable() => TickController.Add(this);
    private void OnDisable() => TickController.Remove(this);

    public void Start() {
        rb = GetComponent<Rigidbody2D>();
        GameSettings.Instance.hero = transform;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Platform") FlappyEvents.Instance.EndGame();
    }

    public void Tick() {
        Move();
        Jump();
        UpdateDistance();
        TryDead();
    }

    private void Move() {
        vector = rb.velocity;
        vector.x = moveSpeed;
        rb.velocity = vector;
    }

    private void Jump() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            rb.velocity = Vector2.zero;
            rb.AddForce(Vector2.up * powerJump);
        }
    }

    private void UpdateDistance() {
        if (FlappyHolder.Instance.MaxDistance < transform.position.x) {
            FlappyHolder.Instance.MaxDistance = transform.position.x;
        }
    }

    private void TryDead() {
        if(transform.position.y >= GameSettings.Instance.borders.top ||
           transform.position.y <= GameSettings.Instance.borders.bottom) {
            FlappyEvents.Instance.EndGame();
        }
    }
}
