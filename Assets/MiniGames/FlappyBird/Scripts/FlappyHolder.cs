public class FlappyHolder : SceneSingleton<FlappyHolder> {
    protected void Start() {
        GameEvents.Instance.OnAddCoin += () => ++Coins;
    }

    private float maxDistance;
    public float MaxDistance {
        get => maxDistance;
        set {
            maxDistance = value;
            FlappyEvents.Instance.UpdateDistance((int) value);
        }
    }

    private int coins;
    public int Coins {
        get => coins;
        set {
            coins = value;
            FlappyEvents.Instance.UpdateCoins(value);
        }
    }
}
