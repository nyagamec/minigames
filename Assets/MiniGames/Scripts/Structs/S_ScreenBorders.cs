[System.Serializable]
public struct S_ScreenBorders {
    public float top;
    public float left;
    public float right;
    public float bottom;
}
