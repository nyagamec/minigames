using System;

public class GameEvents : Singleton<GameEvents> {
    public void UpdateDoodleRecordDistance(int distance) => OnUpdateDoodleRecordDistance?.Invoke(distance);
    public event Action<int> OnUpdateDoodleRecordDistance;
    public void AddCoin() => OnAddCoin?.Invoke();
    public event Action OnAddCoin;
    public void UpdateDoodleRecordCoins(int count) => OnUpdateDoodleRecordCoins?.Invoke(count);
    public event Action<int> OnUpdateDoodleRecordCoins;
}
