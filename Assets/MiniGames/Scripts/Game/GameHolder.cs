public class GameHolder : SceneSingleton<GameHolder> {
    private int doodleRecordDistance;
    public int DoodleRecordDistance {
        get => doodleRecordDistance;
        set {
            doodleRecordDistance = value;
            GameEvents.Instance.UpdateDoodleRecordDistance(value);
        }
    }

    private int doodleRecordCoins;
    public int DoodleRecordCoins {
        get => doodleRecordCoins;
        set {
            doodleRecordCoins = value;
            GameEvents.Instance.UpdateDoodleRecordCoins(value);
        }
    }


}
