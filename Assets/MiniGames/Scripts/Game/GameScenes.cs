using UnityEngine.SceneManagement;

public class GameScenes : Singleton<GameScenes> {
    public SceneName Scene {
        set {
            switch (value) {
                case SceneName.Hub: OpenHub(); break;
                case SceneName.FlappyBird: OpenFlappyBird(); break;
                case SceneName.DoodleJump: OpenDoodleJump(); break;
            }
        }
    }

    public void OpenHub() {
        SceneManager.LoadScene("Hub");
    }

    public void OpenFlappyBird() {
        SceneManager.LoadScene("FlappyBird");
    }

    public void OpenDoodleJump() {
        SceneManager.LoadScene("DoodleJump");
    }

    public enum SceneName {
        Hub,
        FlappyBird,
        DoodleJump
    }
}
