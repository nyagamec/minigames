using UnityEngine;

public class CoinsController : MonoBehaviour {
    [SerializeField] private Coin prefab;
    [SerializeField] private float spawnDistance;
    [SerializeField] private int count;
    [SerializeField] private Orientation orientation;

    private Vector2 nextPosition = new();
    private Coin coin;

    private void Start() {
        switch (orientation) {
            case Orientation.Horizontal:
                nextPosition.y = Random.Range(GameSettings.Instance.borders.bottom + prefab.transform.localScale.y / 2,
                                              GameSettings.Instance.borders.top - prefab.transform.localScale.y / 2);
                break;
            case Orientation.Vertical:
                nextPosition.x = Random.Range(GameSettings.Instance.borders.left + prefab.transform.localScale.x / 2,
                                              GameSettings.Instance.borders.right - prefab.transform.localScale.x / 2);
                break;
        }

        for (int i = 0; i < count; ++i) {
            SpawnCoin();
        }
    }

    private void SpawnCoin() {
        coin = Instantiate(prefab);
        coin.transform.position = nextPosition;
        coin.OnHide += SpawnCoin;
        switch (orientation) {
            case Orientation.Horizontal:
                NewHorizontalPosition();
                break;
            case Orientation.Vertical:
                NewVerticalPosition();
                break;
        }
    }

    private void NewVerticalPosition() {
        nextPosition.y = Random.Range(GameSettings.Instance.borders.top - spawnDistance, GameSettings.Instance.borders.top);
        nextPosition.x = Random.Range(GameSettings.Instance.borders.left + coin.transform.localScale.x / 2,
                                      GameSettings.Instance.borders.right - coin.transform.localScale.x / 2);
    }

    private void NewHorizontalPosition() {
        nextPosition.x = Random.Range(GameSettings.Instance.borders.right - spawnDistance, GameSettings.Instance.borders.right);
        nextPosition.y = Random.Range(GameSettings.Instance.borders.bottom + coin.transform.localScale.y / 2,
                                      GameSettings.Instance.borders.top - coin.transform.localScale.y / 2);
    }
}
