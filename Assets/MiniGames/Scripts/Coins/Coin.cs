using UnityEngine;
using System;

public class Coin : MonoBehaviour, ITick {
    [SerializeField] private float distanceToHide;

    private void OnEnable() => TickController.Add(this);
    private void OnDisable() => TickController.Remove(this);
    
    public Action OnHide;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Coin" || collision.tag == "Platform") {
            Hide();
        }
        if(collision.tag == "Hero") {
            GameEvents.Instance.AddCoin();
            Hide();
        }
    }

    public void Tick() {
        TryHide();
    }

    private void TryHide() {
        if (Vector2.Distance(transform.position, GameSettings.Instance.hero.position) >  distanceToHide) {
            Hide();
        }
    }

    private void Hide() {
        OnHide?.Invoke();
        Destroy(gameObject);
    }
}
