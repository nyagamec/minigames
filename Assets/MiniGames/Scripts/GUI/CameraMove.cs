using UnityEngine;

namespace MiniGames {
    public class CameraMove : MonoBehaviour, ITick {
        [SerializeField] private Orientation direction;
        private Vector3 position;

        private void OnEnable() => TickController.Add(this);
        private void OnDisable() => TickController.Remove(this);

        public void Tick() {
            if (GameSettings.Instance?.hero == null) return;
            position = transform.position;
            switch (direction) {
                case Orientation.Horizontal:
                    position.x = GameSettings.Instance.hero.transform.position.x;
                    break;
                case Orientation.Vertical:
                    position.y = GameSettings.Instance.hero.transform.position.y;
                    break;
            }
            transform.position = position;
        }
    }
}
