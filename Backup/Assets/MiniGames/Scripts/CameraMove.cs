using UnityEngine;

namespace MiniGames {
    public class CameraMove : MonoBehaviour {
        [SerializeField] private Orientation direction;
        private Vector3 position;

        void Update() {
            position = transform.position;
            switch (direction) {
                case Orientation.Horizontal:
                    // TODO
                    //position.x = GameSettings.Instance.hero.transform.position.x;
                    break;
                case Orientation.Vertical:
                    //position.y = GameSettings.Instance.hero.transform.position.y;
                    break;
            }
            transform.position = position;
        }
    }
}
