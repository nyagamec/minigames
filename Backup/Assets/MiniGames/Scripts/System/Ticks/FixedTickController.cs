using System.Collections.Generic;

public class FixedTickController : SceneSingleton<FixedTickController> {
    private static List<IFixedTick> list = new List<IFixedTick>();

    public static void Add(IFixedTick val) => list.Add(val);
    public static void Remove(IFixedTick val) => list.Remove(val);

    private void FixedUpdate() {
        for (int i = 0; i < list.Count; ++i) {
            list[i].FixedTick();
        }
    }
}
