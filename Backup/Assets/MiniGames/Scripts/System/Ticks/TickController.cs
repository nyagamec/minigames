using System.Collections.Generic;

public class TickController : SceneSingleton<TickController> {
    private static List<ITick> list = new List<ITick>();

    public static void Add(ITick val) => list.Add(val);
    public static void Remove(ITick val) => list.Remove(val);

    private void Update() {
        for(int i = 0; i < list.Count; ++i) {
            list[i].Tick();
        }
    }
}
