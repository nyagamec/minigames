using UnityEngine;
using TMPro;

public class HUD : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI distanceTxt;
    [SerializeField] private TextMeshProUGUI coinsTxt;
    [SerializeField] private TextMeshProUGUI gameOverTxt;

    void Start() {
        GameEvents.Instance.OnUpdateDistance += UpdateDistance;
        GameEvents.Instance.OnUpdateCoins += UpdateCoins;
        GameEvents.Instance.OnEndGame += ShowGameOver;
    }

    private void UpdateDistance(float distance) {
        distanceTxt.text = distance.ToString();
    }

    private void UpdateCoins(int coins) {
        coinsTxt.text = coins.ToString();
    }

    private void ShowGameOver() {
        gameObject.SetActive(true);
    }
}
