using UnityEngine;
using System;

public class GameEvents : SceneSingleton<GameEvents> {
    public void UpdateDistance(float distance) => OnUpdateDistance?.Invoke(distance);
    public event Action<float> OnUpdateDistance;
    public void UpdateCoins(int count) => OnUpdateCoins?.Invoke(count);
    public event Action<int> OnUpdateCoins;
    public void EndGame() => OnEndGame?.Invoke();
    public event Action OnEndGame;
}
