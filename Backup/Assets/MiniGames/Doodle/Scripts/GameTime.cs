using System.Threading.Tasks;
using UnityEngine;

public class GameTime : Singleton<GameTime> {
    [SerializeField] private AnimationCurve curve;
    [SerializeField] private TimeType timeType;

    public TimeType Type {
        set {
            timeType = value;
            switch (timeType) {
                case TimeType.MoveByFunction: MoveByFunc(); break;
                case TimeType.MoveByCurve: MoveByCurve(); break;
                case TimeType.Stabilization: Stabilization(); break;
                case TimeType.Stop: Stop(); break;
            }
        }
    }

    private void Awake() {
        Type = timeType;
    }

    private void OnDestroy() {
        Type = TimeType.Stabilization;
    }

    private async void MoveByFunc() {
        Time.timeScale = 1f;
        float time = Time.time;
        float deltaTime = Time.fixedDeltaTime;
        while (timeType == TimeType.MoveByFunction) {
            time += deltaTime;
            Time.timeScale *= 1.0001f;
            print(Time.timeScale);
            await Task.Delay((int)(deltaTime * 1000));
        }
    }

    private async void MoveByCurve() {
        Time.timeScale = curve.Evaluate(0);
        float time = Time.time;
        float deltaTime = Time.fixedDeltaTime;
        while (timeType == TimeType.MoveByCurve && time <= curve.keys[curve.length - 1].time) {
            time += deltaTime;
            Time.timeScale = curve.Evaluate(time);
            await Task.Delay((int)(deltaTime * 1000));
        }
    }

    private void Stabilization() {
        Time.timeScale = 1f;
    }

    private void Stop() {
        Time.timeScale = 0f;
    }

    public enum TimeType {
        MoveByFunction,
        MoveByCurve,
        Stabilization,
        Stop
    }
}
